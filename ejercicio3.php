<?php

    //Abrimos el archivo en lectura
    $descriptor = fopen("elquijote.txt", "r");
    $num = 0;
    $search = "molino";
    $palabra = "";

    //Leemos caracter por caracter y hacemos bucle mientras haya alguno, cuando no haya, sale.
    while (($contenido = fgetc($descriptor)) !== false) { 
        //Comprobamos si hay algún caracter diferente a una letra del abecedario
        if ($contenido === '.' || $contenido === ' ' || $contenido === '\n' || $contenido === '-' || $contenido === '!' || $contenido === '¡' || $contenido === '?' || $contenido === '¿' || $contenido === ':' || $contenido === ';' || $contenido === ',') {
            //Pasamos todo a mayúsculas y hacemos la comparación de la palabra que queremos encontrar con la que hemos creado en el bucle
            if (strtoupper($palabra) === strtoupper($search)) {
                $num++;
            }
    
            $palabra = "";
        //Si no encuentra un caracter de los buscamos, sigue metiendo el caracter en la variable $palabra
        } else {
            $palabra = $palabra . $contenido;
        }
    }

    echo "Las veces que ha salido la palabra " . $search . " han sido: " . $num . "\n";


    fclose($descriptor);


?>